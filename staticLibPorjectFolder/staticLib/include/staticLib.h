#ifndef __STATICLIB_H__
#define __STATICLIB_H__

class CStaticLibTest
{
public:
    CStaticLibTest() = default;
    ~CStaticLibTest() = default;

public:
    void TestFunction();
};


#endif // __STATICLIB_H__