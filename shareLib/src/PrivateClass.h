#ifndef __PRIVATECLASS_H__
#define __PRIVATECLASS_H__

#include <QWidget>

#include "../include/shareLibExport.h"

namespace Ui
{
    class CPrivateClass;
}

class SHARELIB_EXPORT CPrivateClass: public QWidget
{
public:
    explicit CPrivateClass(QWidget* pParent = nullptr);
    ~CPrivateClass();

private:
    void _Initialize(); 

private:
    Ui::CPrivateClass* ui = nullptr;
};

#endif // __PRIVATECLASS_H__