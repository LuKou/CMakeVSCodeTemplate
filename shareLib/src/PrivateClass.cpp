
#include "PrivateClass.h"

#include <QTextCodec>

#include "ui_PrivateClass.h"

CPrivateClass::CPrivateClass(QWidget* pParent) 
    : QWidget(pParent)
    , ui(new Ui::CPrivateClass)
{
    ui->setupUi(this);

    _Initialize();
}

CPrivateClass::~CPrivateClass() 
{
    if(nullptr != ui)
    {
        delete ui;
        ui = nullptr;
    }
}

void CPrivateClass::_Initialize() 
{
    QTextCodec* codec = QTextCodec::codecForName("GB2312");
    QString strText = codec->toUnicode("��������");
    QString strEnText = "test";
    ui->label_direct->setText(strText);
}
