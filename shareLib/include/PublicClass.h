#ifndef __PUBLICCLASS_H__
#define __PUBLICCLASS_H__

#include "shareLibExport.h"

class SHARELIB_EXPORT CPublicClass
{
public:
    CPublicClass() = default;
    ~CPublicClass() = default;

public:
    void Test();
};


#endif // __PUBLICCLASS_H__