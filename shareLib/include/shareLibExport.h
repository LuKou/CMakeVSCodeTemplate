#ifndef __SHARELIBEXPORT_H__
#define __SHARELIBEXPORT_H__

#if defined(WIN32)

#ifdef SHARELIB_EXPORTS
#define SHARELIB_EXPORT __declspec(dllexport)
#else
#define SHARELIB_EXPORT __declspec(dllimport)
#endif

#elif __linux__
#define SHARELIB_EXPORT

#else
#define SHARELIB_EXPORT
#endif


#endif // __SHARELIBEXPORT_H__