#include <QApplication>

#include "../../shareLib/include/PublicClass.h"
#include "../../staticLibPorjectFolder/staticLib/include/staticLib.h"

#include "PrivateClass.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

    CPublicClass oPublicClass;
    oPublicClass.Test();

    CStaticLibTest oStaticTest;
    oStaticTest.TestFunction();

	CPrivateClass oWindow;
	oWindow.show();
	return a.exec();
}