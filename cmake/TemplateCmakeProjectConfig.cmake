if (CMAKE_VERSION VERSION_LESS 3.5.0)
    message(FATAL_ERROR "TemplateCmakeProject requires at least CMake version 3.10.0")
endif()

if (NOT TemplateCmakeProject_FIND_COMPONENTS)
    set(TemplateCmakeProject_NOT_FOUND_MESSAGE "The TemplateCmakeProject package requires at least one component")
    set(TemplateCmakeProject_FOUND False)
    return()
endif()

set(_TemplateCmakeProject_FIND_PARTS_REQUIRED)
if (TemplateCmakeProject_FIND_REQUIRED)
    set(_TemplateCmakeProject_FIND_PARTS_REQUIRED REQUIRED)
endif()

set(_TemplateCmakeProject_FIND_PARTS_QUIET)
if (TemplateCmakeProject_FIND_QUIETLY)
    set(_TemplateCmakeProject_FIND_PARTS_QUIET QUIET)
endif()

get_filename_component(TemplateCmakeProject_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}/../.." ABSOLUTE)

set(_TemplateCmakeProject_NOTFOUND_MESSAGE)

list(APPEND _list_module_Prefix Base)

foreach(module ${TemplateCmakeProject_FIND_COMPONENTS})
    set(_module_not_find TRUE)
    foreach(_prefix ${_list_module_Prefix})
        find_package(${module}
            ${_TemplateCmakeProject_FIND_PARTS_QUIET}
            ${_TemplateCmakeProject_FIND_PARTS_REQUIRED}
            PATHS "${TemplateCmakeProject_INSTALL_PREFIX}/Lib/cmake/${_prefix}/${module}" NO_DEFAULT_PATH
        )
        if (${module}_FOUND)
            set(_module_not_find FALSE)
            break()
        endif()
    endforeach()
    if (_module_not_find)
        if (TemplateCmakeProject_FIND_REQUIRED_${module})
            set(_TemplateCmakeProject_NOTFOUND_MESSAGE "${_TemplateCmakeProject_NOTFOUND_MESSAGE}Failed to find TemplateCmakeProject component \"${module}\" config file at \"${module}Config.cmake\"\n")
        elseif(NOT TemplateCmakeProject_FIND_QUIETLY)
            message(WARNING "Failed to find TemplateCmakeProject component \"${module}\" config file at \"${module}Config.cmake\"")
        endif()
    endif()
endforeach()

if (_TemplateCmakeProject_NOTFOUND_MESSAGE)
    set(TemplateCmakeProject_FOUND_MESSAGE "${_TemplateCmakeProject_NOTFOUND_MESSAGE}")
    set(TemplateCmakeProject_FOUND FALSE)
else()
    if(NOT TemplateCmakeProject_ROOT)
        if(NOT $ENV{TemplateCmakeProject_DIR})
            set(TemplateCmakeProject_ROOT "$ENV{TemplateCmakeProject_DIR}/../../" CACHE PATH "Location of TemplateCmakeProject")
        else()
            set(TemplateCmakeProject_ROOT "${TemplateCmakeProject_DIR}/../../" CACHE PATH "Location of TemplateCmakeProject")
        endif(NOT $ENV{TemplateCmakeProject_DIR})
    endif()
endif()

